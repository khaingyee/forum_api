const passport = require('../config/passportConfig');
const jwt = require('jsonwebtoken');
const User = require('../models/user.js');
const { SECRET_KEY } = require('../config/config.js');

// Register new user
exports.register = (req, res) => {
  const { name, email, password } = req.body;

  // Check if email is already in use
  User.findOne({ email })
    .then(user => {
      if (user) {
        return res.status(400).json({ email: 'Email already in use' });
      } else {
        const newUser = new User({ name, email, password });

        // Save new user to database
        newUser.save()
          .then(user => res.json(user))
          .catch(err => console.log(err));
      }
    });
};

// Log in
exports.login = async (req, res) => {
  const { email, password } = req.body;

  try {
    const user = await User.findOne({ email });
    if (!user) {
      return res.status(400).json({ message: 'Invalid email or password' });
    }

    const isMatch = await user.isValidPassword(password);
    if (!isMatch) {
      return res.status(400).json({ message: 'Invalid email or password' });
    }

    const token = jwt.sign({ userId: user._id }, process.env.JWT_SECRET);
    return res.json({ success: true, token, user });
  } catch (error) {
    console.error(error);
    return res.status(500).json({ message: 'Internal server error' });
  }
};
