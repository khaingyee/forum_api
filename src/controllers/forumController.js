const Post = require('../models/post.js');
const Comment = require('../models/Comment');
const { ObjectId } = require('mongodb');
// Get all posts
const getPosts = async (req, res) => {
  try {
    const posts = await Post.find()
    .populate({
      path: 'author',
      select: 'name',
    })
    .populate({
      path: 'comments',
      populate: {
        path: 'author',
        select: 'name',
      },
    });;
    res.json(posts);
  } catch (err) {
    console.error(err);
    res.status(500).json({ error: 'Internal server error' });
  }
};

// Get post by ID
const getPostById = async (req, res) => {
  try {
    const post = await Post.findById(req.params.id).populate('author').populate('comments');
    if (!post) {
      return res.status(404).json({ error: 'Post not found' });
    }
    res.json(post);
  } catch (err) {
    console.error(err);
    res.status(500).json({ error: 'Internal server error' });
  }
};

// Create new post
const createPost = async (req, res) => {
  const { title, body, author } = req.body;

  const post = new Post({
    title,
    body,
    author,
    image: req.file.path // Add the path to the uploaded image to the post
  });
  try {
    const savedPost = await post.save();
    res.status(201).json(savedPost);
  } catch (err) {
    console.error(err);
    res.status(500).json({ error: 'Internal server error' });
  }
};

// Create new comment for post
const createComment = async (req, res) => {
  const { author, content, post } = req.body;
  postId = new ObjectId(post);
  try {
    const post = await Post.findById(postId);
    if (!post) {
      return res.status(404).json({ error: 'Post not found' });
    }
    const comment = new Comment({
      content,
      author: new ObjectId(author),
      post: postId,
      createdAt: new Date(),
    });
    const savedComment = await comment.save();
    post.comments.push(savedComment._id);
    await post.save();
    res.status(201).json(savedComment);
  } catch (err) {
    console.error(err);
    res.status(500).json({ error: 'Internal server error' });
  }
};

// give reaction
const createLike = async (req, res) => {
  const { author, isLike, post } = req.body;
  try {
    const postData = await Post.findById(new ObjectId(post));
    if (!postData) {
      return res.status(404).json({ error: 'Post not found' });
    }

    if(isLike) {
      postData.likes.push(author);
    }else {
      const objWithIdIndex = postData.likes.findIndex((authorId) => authorId.toString() == author);

      if (objWithIdIndex > -1) {
        postData.likes.splice(objWithIdIndex, 1);
      }
    }
    
    await postData.save();
    res.status(201).json(postData);
  } catch (err) {
    console.error(err);
    res.status(500).json({ error: 'Internal server error' });
  }
};
module.exports = {
  getPosts,
  getPostById,
  createPost,
  createComment,
  createLike
};
