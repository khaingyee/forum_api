const express = require('express');
const router = express.Router();
const passport = require('passport');
const forumController = require('../controllers/forumController');
const multer = require('multer');

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'uploads/');
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + '-' + file.originalname);
  }
});

const upload = multer({ storage: storage });
// Public routes
router.get('/posts', forumController.getPosts);
router.get('/posts/:id', forumController.getPostById);
// Private routes (require authentication using JWT token)
router.post('/posts', passport.authenticate('jwt', { session: false }), upload.single('image'), forumController.createPost);
router.post('/posts/comments', passport.authenticate('jwt', { session: false }), forumController.createComment);
router.post('/posts/like', passport.authenticate('jwt', { session: false }), forumController.createLike);

module.exports = router;
