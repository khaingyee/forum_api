const express = require('express');
const router = express.Router();
const passport = require('passport');
const authController = require('../controllers/authController');

// Public route
router.post('/register', authController.register);

// Public route that authenticates a user and returns a JWT token
router.post('/login', authController.login);

// Private route that requires authentication using a JWT token
//router.get('/profile', passport.authenticate('jwt', { session: false }), authController.profile);

module.exports = router;
