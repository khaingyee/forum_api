module.exports = {
    MONGODB_URI: process.env.MONGODB_URI || 'mongodb://localhost:27017/forum',
    SESSION_SECRET: process.env.SESSION_SECRET || 'secret'
  };
  